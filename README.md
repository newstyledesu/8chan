# 8chan Dark Fagioli

## About
A compact dark theme with mascot for 8chan. The post form is fixed to the side for your convenience. The mascot is base64 encoded due to the site's strict content security policy. If you replace the mascot with your own be sure to encode it here or it wont show up: https://www.base64-image.de/

## Links
Userstyles page: https://userstyles.org/styles/148657/8chan-dark-fagioli-with-mascot

## Screenshot
![](8chan.png)

## Installation

### Step 1:
Install the Stylish or Stylus extension for Firefox or Chrome. Then, either:

### Step 2:
Install from userstyles (https://userstyles.org/styles/148657/8chan-dark-fagioli-with-mascot)  
OR  
Import the *.css file into Stylish or Stylus manually